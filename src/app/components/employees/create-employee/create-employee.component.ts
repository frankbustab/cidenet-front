import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import { EmployeesService } from "../../../services/employees.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-create-employee',
  templateUrl: './create-employee.component.html',
  styleUrls: ['./create-employee.component.css']
})
export class CreateEmployeeComponent implements OnInit {

  constructor( private formBuilder: FormBuilder, public employeeService: EmployeesService, private router: Router) {
    this.createEmployeeFrom = this.formBuilder.group({
      firstName: ['', Validators.required],
      otherNames: '',
      firstLastName: ['', Validators.required],
      secondLastName: ['', Validators.required],
      identificationNumber: ['0', Validators.required],
      identificationType: ['', Validators.required],
      area: ['', Validators.required],
      country: ['', Validators.required],
      entryDate: [new Date(), Validators.required]
    });
  }

  public createEmployeeFrom: FormGroup;
  public areas: any = ['DESARROLLO', 'ADMINISTRACION', 'FINANCIERA', 'COMPRAS', 'INFRAESTRUCTURA', 'OPERACION', 'TALENTO_HUMANO', 'SERVICIOS_VARIOS']
  public identificationTypes: any = ['CEDULA_CIUDADANIA', 'CEDULA_EXTRANJERIA', 'PASAPORTE', 'PERMISO_ESPECIAL']
  public countries: any = ['USA', 'COLOMBIA']

  ngOnInit(): void {
  }

  public isInvalid(input: string ){
    return this.createEmployeeFrom.controls[input].invalid &&
      this.createEmployeeFrom.controls[input].touched;
  }

  public save(){
    if (this.createEmployeeFrom.invalid) {
      return Object.values(this.createEmployeeFrom.controls).forEach(control => {
        control.markAsTouched();
      });
    }

    const data: any = this.createEmployeeFrom;

    this.employeeService.create({
      firstName: this.createEmployeeFrom.controls['firstName'].value,
      otherNames: this.createEmployeeFrom.controls['otherNames'].value,
      firstLastName: this.createEmployeeFrom.controls['firstLastName'].value,
      secondLastName: this.createEmployeeFrom.controls['secondLastName'].value,
      identificationNumber: this.createEmployeeFrom.controls['identificationNumber'].value,
      identificationType: this.createEmployeeFrom.controls['identificationType'].value,
      area: this.createEmployeeFrom.controls['area'].value,
      country: this.createEmployeeFrom.controls['country'].value,
      entryDate: this.createEmployeeFrom.controls['entryDate'].value
    }).subscribe(resp => {
      window.location.reload();
    })

  }

  public cancel(){
    this.router.navigateByUrl('');
    window.location.reload();

  }
}

import { Component, OnInit } from '@angular/core';
import { EmployeesService } from "../../services/employees.service";
import {MatDialog} from '@angular/material/dialog';
import {EditEmployeeComponent} from "./edit-employee/edit-employee.component";
import {ShowEmployeeComponent} from "./show-employee/show-employee.component";
import {CreateEmployeeComponent} from "./create-employee/create-employee.component";

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  constructor(public employeesService: EmployeesService, private dialog: MatDialog) { }

  public employees: any = [];

  public pageSize = 10;
  public page = 1;
  public dataTable: any = [];

  ngOnInit(): void {
    this.loadData();
    this.refreshTable();
  }

  public loadData(){
    this.employeesService.all().subscribe(resp => {
      this.employees = resp;
      this.dataTable = resp.slice(
        (this.page - 1) * this.pageSize,
        (this.page - 1) * this.pageSize + this.pageSize,
      );
    });
  }

  public refreshTable(){
    this.dataTable = this.employees.slice(
      (this.page - 1) * this.pageSize,
      (this.page - 1) * this.pageSize + this.pageSize,
    );
  }

  public edit(employee: any){
    this.dialog.open(EditEmployeeComponent, { data: { employee: employee }});
  }

  public  show(employee: any){
    this.dialog.open(ShowEmployeeComponent, { data: { employee: employee}});
  }

  public create(){
    this.dialog.open(CreateEmployeeComponent);
  }

  public delete(id: number){
    this.employeesService.delete(id).subscribe(resp =>{
      this.loadData();
    });
  }
}

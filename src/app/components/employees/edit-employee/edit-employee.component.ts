import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import { EmployeesService } from "../../../services/employees.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-edit-employee',
  templateUrl: './edit-employee.component.html',
  styleUrls: ['./edit-employee.component.css']
})
export class EditEmployeeComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: {employee: any}, private formBuilder: FormBuilder, public employeeService: EmployeesService,  private router: Router) {
    this.editEmployeeFrom = this.formBuilder.group({
      firstName: ['', Validators.required],
      otherNames: [''],
      firstLastName: ['', Validators.required],
      secondLastName: ['', Validators.required],
      identificationNumber: ['0', Validators.required],
      identificationType: ['', Validators.required],
      area: ['', Validators.required],
      country: ['', Validators.required],
      entryDate: [Date, Validators.required]
    });
  }

  public areas: any = ['DESARROLLO', 'ADMINISTRACION', 'FINANCIERA', 'COMPRAS', 'INFRAESTRUCTURA', 'OPERACION', 'TALENTO_HUMANO', 'SERVICIOS_VARIOS']
  public identificationTypes: any = ['CEDULA_CIUDADANIA', 'CEDULA_EXTRANJERIA', 'PASAPORTE', 'PERMISO_ESPECIAL']
  public countries: any = ['USA', 'COLOMBIA'];
  public descripcion: string = "";

  public editEmployeeFrom: FormGroup;

  ngOnInit(): void {
    if(this.data.employee){
      let employee = this.data.employee
      this.descripcion = employee.firstName + ' ' + employee.firstLastName + '' + employee.identificationNumber
      this.editEmployeeFrom.setValue({
        firstName: employee.firstName,
        otherNames: employee.otherNames,
        firstLastName: employee.firstLastName,
        secondLastName: employee.secondLastName,
        identificationNumber: employee.identificationNumber,
        identificationType: employee.identificationType,
        area: employee.area,
        country: employee.country,
        entryDate: employee.entryDate
      })
    }
  }

  public isInvalid(input: string ){
    return this.editEmployeeFrom.controls[input].invalid &&
      this.editEmployeeFrom.controls[input].touched;
  }

  public update(){
    if (this.editEmployeeFrom.invalid) {
      return Object.values(this.editEmployeeFrom.controls).forEach(control => {
        control.markAsTouched();
      });
    }

    const data: any = this.editEmployeeFrom;

    this.employeeService.update(this.data.employee.id, {
      firstName: this.editEmployeeFrom.controls['firstName'].value,
      otherNames: this.editEmployeeFrom.controls['otherNames'].value,
      firstLastName: this.editEmployeeFrom.controls['firstLastName'].value,
      secondLastName: this.editEmployeeFrom.controls['secondLastName'].value,
      identificationNumber: this.editEmployeeFrom.controls['identificationNumber'].value,
      identificationType: this.editEmployeeFrom.controls['identificationType'].value,
      area: this.editEmployeeFrom.controls['area'].value,
      country: this.editEmployeeFrom.controls['country'].value,
      entryDate: this.editEmployeeFrom.controls['entryDate'].value
    }).subscribe(resp => {
      window.location.reload();
    });


  }

  public cancel(){
   window.location.reload();
  }

}

import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA} from "@angular/material/dialog";
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-show-employee',
  templateUrl: './show-employee.component.html',
  styleUrls: ['./show-employee.component.css'],
  providers: [DatePipe]
})
export class ShowEmployeeComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: {employee: any}) { }

  public employee: any;
  

  ngOnInit(): void {
    this.employee = this.data.employee
  }

}

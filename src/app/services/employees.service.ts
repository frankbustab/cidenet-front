import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {
  public url: string = environment.url + "employees/";

  constructor(private httpClient: HttpClient) { }

  public create(employee: any){
    return this.httpClient.post<any>(this.url, employee, this.getHeaders());
  }

  public all(){
    return this.httpClient.get(this.url, this.getHeaders());
  }

  public update(id: number, employee: any){
    return this.httpClient.put(`${this.url}${id}`, employee, this.getHeaders());
  }

  public byId(id: number){
    this.httpClient.get(`${this.url}${id}`, this.getHeaders());
  }

  public delete(id: number){
    return this.httpClient.delete(`${this.url}${id}`, this.getHeaders());
  }

  private getHeaders(): any{
    const httpOptions = {
      headers: new HttpHeaders({
        'ContentType': 'application/json'
      })
    };
    return httpOptions
  }
}

import {  ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EmployeesComponent } from "./components/employees/employees.component";

const appRoutes: Routes = [
  { path: '', component: EmployeesComponent },
  { path: 'inicio', component: EmployeesComponent }
];

export const appRoutingProviders: any [] = [];
export const routing: ModuleWithProviders<any> = RouterModule.forRoot(appRoutes);

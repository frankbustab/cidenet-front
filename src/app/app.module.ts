import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { routing, appRoutingProviders } from "./app-routing.module";
import { HttpClientModule } from '@angular/common/http'

import { AppComponent } from './app.component';
import { EmployeesComponent } from './components/employees/employees.component';
import { EmployeesService } from "./services/employees.service";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module'
import {EditEmployeeComponent} from "./components/employees/edit-employee/edit-employee.component";
import {CreateEmployeeComponent} from "./components/employees/create-employee/create-employee.component";
import {ShowEmployeeComponent} from "./components/employees/show-employee/show-employee.component";


@NgModule({
  declarations: [
    AppComponent,
    EmployeesComponent,
    EditEmployeeComponent,
    CreateEmployeeComponent,
    ShowEmployeeComponent
  ],
  imports: [
    BrowserModule,
    routing,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MaterialModule,
    NgbModule,
    BrowserAnimationsModule
  ],
  providers: [
    appRoutingProviders,
    EmployeesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
